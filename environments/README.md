After creating the environment, you can upload it with this:

    knife environment from file environments/acme_staging.json

For more information on environments, see the Chef wiki page:

* https://docs.chef.io/environments.html
