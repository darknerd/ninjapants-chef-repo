#
# Cookbook:: dk_nginx
# Spec:: config
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

require 'spec_helper'

describe 'dk_nginx::config' do
  context 'Congigures NGINX settings on Ubuntu 14.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '14.04').converge(described_recipe)
    end

    let(:sites_dirs) { %w(sites-available sites-enabled) }

    it 'should converge successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'should creates nginx directories' do
      sites_dirs.each do |dir|
        expect(chef_run).to create_directory("/etc/nginx/#{dir}").with(
          user: 'root',
          group: 'root',
          mode: '0755'
        )
      end
    end

    it 'should remove /etc/nginx/config.d/default.conf' do
      expect(chef_run).to delete_file('/etc/nginx/config.d/default.conf')
    end

    it 'should create /etc/nginx/nginx.conf' do
      expect(chef_run).to create_cookbook_file('/etc/nginx/nginx.conf').with(
        user: 'root',
        group: 'root',
        mode: '0755'
      )
    end

    it 'should create /var/www directory' do
      expect(chef_run).to create_directory('/var/www').with(
         user: 'www-data',
         group: 'www-data',
         mode: '0755'
       )
    end
  end
end
