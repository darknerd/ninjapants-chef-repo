# dk_elasticsearch

Installs and configures basic elasticsearch without any sites installed.

## Reference

* https://www.elastic.co/guide/en/elasticsearch/reference/6.0/deb.html
* https://docs.chef.io/config_yml_kitchen.html#chef-splunk-cookbook
* https://docs.chef.io/berkshelf.html
