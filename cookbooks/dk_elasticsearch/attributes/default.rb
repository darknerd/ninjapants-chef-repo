
if node['virtualization']['system'] == 'vbox'
  ipaddress = node['network']['interfaces']['eth1']['addresses'].detect do |k, v|
    v['family'] == "inet"
  end.first.to_s
elsif node.key?('gce')
  ipaddress = node['ipaddress'] # eth0
elsif node.key?('ec2')
  ipaddress = node['ipaddress'] # eth0
else
  ipaddress = '_local_'
end

default['dk_elasticsearch']['jvm_heap_size'] = '512m'
default['dk_elasticsearch']['cluster_name'] = 'dk_cluster'
default['dk_elasticsearch']['network_host'] = ipaddress
default['dk_elasticsearch']['unicast_hosts'] = []
