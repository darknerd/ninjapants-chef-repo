# Inspec test for recipe dk_elasticsearch::default
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

describe port(9200) do
  it { should be_listening }
end

describe command("ohai 2> /dev/null | grep \-P '\"172.16.0.\\d\\d\". {' | sed \-E 's/\"(.*)\": \\{/\\1/' | awk '{print $1}' > ipaddress") do
  its('exit_status') { should eq 0 }
end

describe command('curl "$(cat ipaddress):9200/_cluster/health?pretty" > cluster_health.json') do
  its('exit_status') { should eq 0 }
end

describe json('/home/vagrant/cluster_health.json') do
  its('cluster_name') { should eq 'dk_cluster' }
  its('number_of_nodes') { should eq 3 }
  its('number_of_data_nodes') { should eq 3 }
  its('status') { should eq 'green' }
end
