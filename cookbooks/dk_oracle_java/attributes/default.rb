default['dk_oracle_java']['docroot'] = '/var/www/java'
default['dk_oracle_java']['local_repo'] = nil
default['dk_oracle_java']['version'] = '8u171'
default['dk_oracle_java']['javas'] = {
  '8u161' => {
    'build' => 'b12',
    'hashcode' => '2f38c3b165be4555a1fa6e98c45e0808',
  },
  '8u162' => {
    'build' => 'b12',
    'hashcode' => '0da788060d494f5095bf8624735fa2f1',
  },
  '8u171' => {
    'build' => 'b11',
    'hashcode' => '512cd62ec5174c3487ac17c61aaa89e8',
  },
}
