#
# Cookbook:: dk_oracle_java
# Spec:: install
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

require 'spec_helper'

describe 'dk_oracle_java::install' do
  context 'When all attributes are default, installs Oracle Java 8 on Ubuntu 14.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '14.04') do |node|
        node.normal['dk_oracle_java']['local_repo'] = '1.2.3.4'
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'installs prerequisite packages' do
      expect(chef_run).to install_package %w(python-software-properties debconf-utils)
    end

    it 'adds Oracle Java 8 debian repository' do
      expect(chef_run).to add_apt_repository('webupd8team-java').with(
        uri: 'ppa:webupd8team/java'
      )
    end

    it 'agrees to Oracle Java 8 license agreement' do
      name = 'shared/accepted-oracle-license-v1-1'
      %w(select seen).each do |type|
        expect(chef_run).to run_execute("echo 'oracle-java8-installer #{name} #{type} true' | debconf-set-selections")
      end
    end

    # Need to test both true/false conditions
    it "creates HostFile Entry" do
      expect(chef_run).to create_hostsfile_entry('local_repo_entry').with(
        ip_address: '1.2.3.4',
        hostname: 'download.oracle.com'
      )
    end

    it 'installs Oracle Java 8' do
      expect(chef_run).to install_package 'oracle-java8-installer'
    end
  end
end
