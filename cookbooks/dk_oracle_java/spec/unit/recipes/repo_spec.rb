#
# Cookbook:: dk_oracle_java
# Spec:: repo
#
# The MIT License (MIT)
#
# Copyright:: 2018, Joaquin Menchaca
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

require 'spec_helper'

describe 'dk_oracle_java::repo' do
  context 'Configures NGiNX site on Ubuntu 14.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '14.04') do |node|
        node.normal['dk_oracle_java']['local_repo'] = nil
        node.normal['dk_oracle_java']['docroot'] = '/var/www/java'
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates java repo doc root' do
      docroot = chef_run.node['dk_oracle_java']['docroot'].to_s
      expect(chef_run).to create_directory(docroot).with(
        user: 'www-data',
        group: 'www-data',
        mode: '0755'
      )
    end

    it 'fetches oracle jdk' do
      expect(chef_run).to run_execute('fetch oracle jdk')
    end

    it 'changes ownership' do
      docroot = chef_run.node['dk_oracle_java']['docroot']
      expect(chef_run).to run_execute("chown #{docroot} to www-data" )
    end

    it 'creates nginx "download.oracle.com" site config from template' do
      expect(chef_run).to create_template('/etc/nginx/sites-available/download.oracle.com')
    end

    it 'enables nginx "download.oracle.com" site' do
      source = '/etc/nginx/sites-available/download.oracle.com'
      target = '/etc/nginx/sites-enabled/download.oracle.com'
      expect(chef_run).to create_link(target).with(to: source)
    end

    it 'does nothing with a service' do
      expect(chef_run.service('nginx')).to do_nothing
    end
  end
end
