# **Overview**

This is a full Chef Repository that includes cookbooks, roles, configuration files and other artifacts for managing systems with Chef.  

In order to use this in a *staging* or *production* environment, you will need to use a Chef Server:

  * Installed OPD within your infrastructure, or
  * Online service (https://api.chef.io/) using free tier.

# **Repository Directories**

The repository has areas that you may need to configure to make this work with your particular environment:

  * `./chef` - configuration that references your Chef Server account *you create this*
  * `cookbooks/` - cookbooks to install things on systems
  * `data-bags/` - flat file database (key/value in json)
  * `nodes`- one configuration per system, created with knife bootstrap *this is created automatically*
  * `roles` - group cookbooks and recipes together for a single role
  * `environments/` - contain environment specific settings, like development, staging, production.

# **Configuration**

You need to create a configuration file that references your account settings on a Chef Server.  This file is stored in `.chef/knife.rb`.

Here is an example file:

```ruby
chef_server_user = 'jdoe' # example
chef_server_org  = 'acme' # example

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                chef_server_user.to_s
client_key               "#{current_dir}/#{chef_server_user}.pem"
chef_server_url          "https://api.chef.io/organizations/#{chef_server_org}"
cookbook_path            ["#{current_dir}/../cookbooks"]
```

# **Additional Steps**

## **Upload Cookbooks**

After you completed the required configuration, you can run through the following:

```bash
# upload berkshelf cookbooks (installed from supermarket)
pushd cookbooks/$cookbook
berks install
berks upload
popd
# upload site cookbooks
for cookbook in cookbooks/dk_*; do knife upload ${cookbook##*/}; done
```

For *development* environment with `knife-zero`, to the following:

```bash
cat <<-EOF > Berksfile
source 'https://supermarket.chef.io'

$(grep -Rh 'depends' cookbooks/* | sed 's/depends/cookbook/')
EOF

berks install
berks vendor
```

## **Upload Roles**

```bash
for role in roles/*.json; do knife role from file $role; done
```

## **Upload Environment**

You should have the target systems already created (EC2, GCE, etc) and configured with a shared secret key used for configuration.  You can copy from one of the existing environment files.  You will need to specify the internal IP address of the nodes where required.

Once crafted, then you would upload this file with something like this:

```bash
knife from file upload environments/acme_prod.json
```

## **Bootstrap Some Nodes**

From our staging or production environment, we need the list of names and their IP address that is use for SSH. We should be able to access them with our installed key, for example:

```bash
ssh ubuntu@35.190.176.254 -i /path/to/key
```

In this example scenario, we have the following systems:

* `dk-prod-tools`: `35.190.150.211`
* `dk-prod-es-01`: `35.190.176.254`
* `dk-prod-es-02`: `35.190.163.175`
* `dk-prod-es-03`: `35.185.56.158`

With these systems, we can bootstrap using this.

```bash
# bootstrap tool server
SYSTEM='dk-prod-tools'
IPADDR='35.190.150.211'
knife bootstrap ${IPADDR} \
 --node-name ${SYSTEM} \
 --environment 'acme_prod' \
 --run-list 'role[java_repo]' \
 --ssh-user 'ubuntu' \
 --sudo \
 --identity-file /path/to/key

# bootstrap elasticsearch nodes
declare -A SYSTEMS
SYSTEMS+=( ["dk-prod-es-01"]="35.190.176.254" ["dk-prod-es-02"]="35.190.163.175" ["dk-prod-es-03"]="35.185.56.158" )
for SYSTEM in "${!SYSTEMS[@]}"; do
  knife bootstrap ${SYSTEMS[$SYSTEM]} \
   --node-name ${SYSTEM} \
   --environment 'acme_prod' \
   --run-list 'role[elasticsearch]' \
   --ssh-user 'ubuntu' \
   --sudo \
   --identity-file /path/to/key
done
```
